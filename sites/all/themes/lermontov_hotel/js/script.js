/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {

  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.my_custom_behavior = {
    attach: function (context, settings) {

      $(".webform-datepicker select.month").val(1);
      $(".webform-datepicker select.day").val(1);
      $(".webform-datepicker select.year").val(1);
    }
  };
  $(document).ready(function() {
    $('#block-menu-menu-header-menu, .block-menu-menu-header-menu-en').prepend("<div class='burger'><span></span><span></span><span></span><span></span><span></span><span></span></div>");
    $('.burger').click(function () {
       $(this).toggleClass('open');
       $(this).siblings('.menu').toggleClass('show');
    });
    $('body').prepend('<div class="lermontov-navigation"></div>');

    $(window).scroll(function(){
      if ($(this).scrollTop() > 200) {
        $('.lermontov-navigation').fadeIn();
      } else {
        $('.lermontov-navigation').fadeOut();
      }
    });

    $('.lermontov-navigation').click(function(){
      $("html, body").animate({ scrollTop: 0 }, 1500);
      return false;
    });
  });


})(jQuery, Drupal, this, this.document);
