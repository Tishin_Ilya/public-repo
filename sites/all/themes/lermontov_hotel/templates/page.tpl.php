<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div class="layout-main">

  <header class="header" role="banner">
      <?php print render($page['headline']); ?>
      <?php print render($page['header']); ?>
      <?php if(!drupal_is_front_page()) print $breadcrumb; ?>

      <?php print render($page['slider']); ?>
  </header>

  <div class="layout-main layout-main">
		<?php
    	$content_class = 'some-content-class';
    ?>

    <main class="<?php print $content_class; ?>" role="main">
      <?php print render($page['highlighted']); ?>

      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['content']); ?>
    </main>

    <div class="layout-main layout-main">
      <?php print render($page['navigation']); ?>

    </div>
  </div>

  <?php print render($page['footer']); ?>

</div>

<?php print render($page['bottom']); ?>
