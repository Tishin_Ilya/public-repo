<?php
/**
 * @file
 * Returns HTML for a region.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728112
 */
?>
<?php if ($content): ?>
  <div id="<?php print $region_id; ?>" class="<?php print $classes; ?>">
    <?php print $content; ?>
  </div>
<?php endif; ?>
