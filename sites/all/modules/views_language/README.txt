About
--------------
This module provides enhancement of the "Content: Language" field in views.
It has an option for choosing the Language options under the Language settings. 
The "Language name" is the default option, that can be changed as Language code, 
Native language, Language prefix etc.
So the added "Content: Language" field value should be changed 
according to the selected Language option.

Usage
-----
Add "field: Content: Language"
Choose Language option from the Language settings

Dependencies
------------
Views
