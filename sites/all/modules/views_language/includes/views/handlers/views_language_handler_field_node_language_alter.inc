<?php

/**
 * @file
 * Definition of views_language_handler_field_node_language_alter.
 */

/**
 * Field handler to translate a language into its readable form.
 *
 * @ingroup views_field_handlers
 */
class views_language_handler_field_node_language_alter extends views_handler_field_node_language {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['lang_settings'] = array(
      'contains' => array(
        'language_options' => array('default' => 'name'),
      ),
    );
    return $options;
  }

  /**
   * Selection option form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['lang_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Language settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 99,
    );
    $form['lang_settings']['language_options'] = array(
      '#title' => t('Language options'),
      '#type' => 'select',
      '#options' => array(
        'code' => t('Language code'),
        'name' => t('Language name'),
        'native' => t('Native language'),
        'domain' => t('Language domain'),
        'direction' => t('Language direction'),
        'prefix' => t('Language prefix'),
      ),
      '#default_value' => $this->options['lang_settings']['language_options'],
      '#description' => t('Selected language option will be displayed'),
    );
    unset($form['native_language']);
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $languages = views_language_lang_list(empty($this->options['lang_settings']['language_options']) ? 'name' : $this->options['lang_settings']['language_options']);
    $value = $this->get_value($values);
    $value = isset($languages[$value]) ? $languages[$value] : '';
    return $this->render_link($value, $values);
  }

}
